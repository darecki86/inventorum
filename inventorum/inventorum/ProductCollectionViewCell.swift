//
//  ProductCollectionViewCell.swift
//  inventorum
//
//  Created by Dariusz Cieśla on 29.10.2015.
//  Copyright © 2015 Dariusz Cieśla. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var safetyStockLabel: UILabel!
    @IBOutlet weak var reorderLevelLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var grossPriceLabel: UILabel!
    
}
