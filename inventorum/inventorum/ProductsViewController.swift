//
//  ProductsViewController.swift
//  inventorum
//
//  Created by Dariusz Cieśla on 29.10.2015.
//  Copyright © 2015 Dariusz Cieśla. All rights reserved.
//

import UIKit

class ProductsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var productArray = [Product]()
    var page = 1
    var fetchingPossible = true
    
    let resultsPerPage = 50
    let refetchLimit = 100
    
    private let reuseIdentifier = "ProductCellIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchNextPage()
    }
        
    func fetchNextPage() {
        if !fetchingPossible {
            return
        }
        fetchingPossible = false
        
        NetworkManager().fetchListOfProducts(limit: resultsPerPage, page: page) { products in
            self.fetchingPossible = true
            
            guard let products = products else {
                print("Request failed")
                return
            }
            
            self.productArray.appendContentsOf(products)
            dispatch_async(dispatch_get_main_queue(), {
                self.collectionView.reloadData()
            })
            
            self.page++
        }
    }
    
    // MARK - Collection view data source
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! ProductCollectionViewCell
        let product = productArray[indexPath.row]
        cell.nameLabel.text = product.name
        cell.safetyStockLabel.text = "\(product.safetyStock)"
        cell.quantityLabel.text = "\(product.quantity)"
        cell.reorderLevelLabel.text = "\(product.reorderLevel)"
        cell.grossPriceLabel.text = "\(product.grossPrice)"
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        if productArray.count - indexPath.row < refetchLimit {
            fetchNextPage()
        }
    }

    // MARK - Flow layout
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: 100)
    }
    
    // MARK - 
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        self.collectionView.reloadData()
    }
}

