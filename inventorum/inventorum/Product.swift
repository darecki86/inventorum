//
//  Product.swift
//  inventorum
//
//  Created by Dariusz Cieśla on 29.10.2015.
//  Copyright © 2015 Dariusz Cieśla. All rights reserved.
//

import Foundation

class Product {
    
    var name: String = ""
    var quantity: Double = 0
    var reorderLevel: Double = 0
    var safetyStock: Double = 0
    var grossPrice: Double = 0
    
    init(dictionary: [String: AnyObject]) {
        if let name = dictionary["name"] {
            self.name = name as! String
        }
        if let quantity = dictionary["quantity"]?.doubleValue {
            self.quantity = quantity
        }
        if let reorderLevel = dictionary["reorder_level"]?.doubleValue {
            self.reorderLevel = reorderLevel
        }
        if let safetyStock = dictionary["safety_stock"]?.doubleValue {
            self.safetyStock = safetyStock
        }
        if let grossPrice = dictionary["gross_price"]?.doubleValue {
            self.grossPrice = grossPrice
        }
    }
}