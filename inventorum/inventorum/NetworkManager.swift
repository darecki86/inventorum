//
//  NetworkManager.swift
//  inventorum
//
//  Created by Dariusz Cieśla on 29.10.2015.
//  Copyright © 2015 Dariusz Cieśla. All rights reserved.
//

import Foundation

class NetworkManager {
    
    private let baseURL = "https://app.inventorum.net/api"
    
    private lazy var session: NSURLSession = {
        let sessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        sessionConfiguration.HTTPAdditionalHeaders = [
            "Authorization": "Bearer WSspLPc6TkD61Blx9Ft35G1jYpqW6p",
            "X-Api-Version": "10"
        ]
        return NSURLSession(configuration: sessionConfiguration)
    }()
    
    
    func fetchListOfProducts(limit limit: Int = 20, page: Int = 1, completion: (products: [Product]?) -> Void) {
        let url = NSURL(string: "\(baseURL)/products/?limit=\(limit)&page=\(page)")
        let task = session.dataTaskWithURL(url!) { (data, response, error) in
            do {
                guard let data = data else {
                    completion(products: nil)
                    return
                }
                let json = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
                guard let dataJson = json["data"] as? [[String: AnyObject]] else {
                    completion(products: nil)
                    return
                }
                
                var parsedProducts = [Product]()
                for productJson in dataJson {
                    let product = Product(dictionary: productJson)
                    parsedProducts.append(product)
                }
                completion(products: parsedProducts)
            } catch let error {
                completion(products: nil)
                print(error)
            }
        }
        task.resume()
    }
    
}